\version "2.16.2"

\header {
  title = "Les copains d'abord"
  instrument = "Guitare et chant"
  composer = "Georges Brassens"
  tagline = "stepan@ithaca.fr"
}

<<
   \chords{
     \set chordChanges = ##t
     \skip 1 d1 d1 d1 d1 e1:7 e1:7 e1:7 e1:7 g1 g1 fis1 fis1:7 
     b1:m e2:7 a2:7 d:1
   }

  
  \relative c' {
    #(set-accidental-style 'default 'Voice)
    \autoBeamOff
    \key d \major
    \time 4/4
    r4 d e eis \mark\markup{ \musicglyph #"scripts.segno" }\bar "||" fis e d e fis4. 
    e8 d4 e fis b cis  d cis4. 
    b8 a4 fis e4 b e b e
    d8 fis e4 d4 gis,1( gis8) r8 r8
    ais8 b4 cis d4. d8 cis4 d b2(b8) 
    d8 cis b ais4. fis'8 fis4 eis e!2(e8)
    g8 fis8 e8 d4 d4 d4 b4 \times 2/3 {gis4 fis'4 e4} b4 cis4 d1~ \bar "||" d8 r8
    d e eis \mark\markup{ \musicglyph #"scripts.segno" }\bar "||"
    
  }
  
  \addlyrics {
    \set stanza = #"1. "
    Non, ce n'é -- tait pas le ra -- deau
    De la Mé -- du -- se ce ba -- teau
    Qu'on se le dis' au fond des ports.
    Dis' au fond des ports.
    Il na -- vi -- guait en Pèr' pei -- nard
    Sur la grand ma -- re des ca -- nards
    Et s'app' -- lait les Co -- pains d'a -- bord
    Les Co -- pains d'a -- bord.
    }
  
  \addlyrics {  
    \skip 1 \skip 1 \skip 1   at nec mer -- gi -- tur»
    C'é -- tait pas d'la lit -- té -- ra tur'
    N'en dé -- plaise aux jet -- teurs de sort,
    Aux jet -- teurs de sort
    Son ca -- pi -- taine et ses mat' -- lots
    N'é -- taient pas des en -- fants d'sa -- lauds,
    Mais des a -- mis fran -- co de port,
    Des Co -- pains d'a -- bord. \set stanza = #"2. " Ses «fluc -- tu
    }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 des a -- mis de lux',
    Des pe -- tits Cas -- tor et Pol -- lux,
    Des gens de So -- dome et Go -- morrh'
    So -- dome et Go -- morrh',
    C'é -- tait pas des a -- mis choi -- sis
    Par Mon -- taigne et la Bo -- é -- ti',
    Sur le ventre ils se ta -- paient fort,
    Les co -- pains d'a -- bord. \set stanza = #"3. " C'é -- tait pas
   }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 des an -- ges non plus,
    L'É -- van -- gile, ils l'a -- vaient pas lu,
    Mais ils s'ai -- maient tout's voil's de -- hors,
    Tou -- tes voil's de -- hors,
    Jean, Pier -- re, Paul et com -- pa -- gnie,
    C'é -- tait leur seu -- le li -- ta -- nie,
    Leur Cre -- do, leur Con -- fi -- te -- or
    Aux co -- pains d'a -- bord. \set stanza = #"4. " C'é -- tait pas
   }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 coup de Tra -- fal -- gar,
    C'est l'a -- mi -- tié qui pre -- nait l'quart,
    C'est ell' qui leur mon -- trait le nord,
    Leur mon -- trait le nord.
    Et quand ils é -- taient en dé -- tress',
    Qu'leurs bras lan -- çaient des S. O. S.,
    On au -- rait dit des sé -- ma -- phores,
    Les co -- pains d'a -- bord. \set stanza = #"5. " Au moin -- dre
   }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 vous des bons co -- pains,
    Y'a -- vait pas sou -- vent de la -- pins,
    Quand l'un d'en  -- tre man -- quait à bord,
    C'est qu'il é -- tait mort.
    Oui, mais ja -- mais, au grand ja -- mais,
    Son trou dans l'eau n'se re -- fer -- mait,
    Cent ans a -- près, co -- quin de sort!
    Il man -- quait en -- cor. \set stanza = #"6. " Au ren -- dez
   }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 j'en ai pris beau -- coup,
    Mais le seul qui'ait te -- nu le coup,
    Qui n'ait ja -- mais vi -- ré de bord,
    Mais vi -- ré de bord,
    Na -- vi -- guait en pè -- re pei -- nard
    Sur la grand ma -- re des ca -- nards,
    Et s'app' -- lait les Co -- pains d'a -- bord,
    Les Co -- pains d'a -- bord. \set stanza = #"7. " Des ba -- teaux,     
   }

   \addlyrics {
    \skip 1 \skip 1 \skip 1 j'en ai pris beau -- coup,
    Mais le seul qui'ait te -- nu le coup,
    Qui n'ait ja -- mais vi -- ré de bord,
    Mais vi -- ré de bord,
    Na -- vi -- guait en pè -- re pei -- nard
    Sur la grand ma -- re des ca -- nards,
    Et s'app' -- lait les Co -- pains d'a -- bord,
    Les Co -- pains d'a -- bord. \set stanza = #"8. " Des ba -- teaux,     
   }
>>
